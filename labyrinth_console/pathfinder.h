#ifndef PATHFINDER_H
#define PATHFINDER_H


class PathFinder
{
public:
    char **find_path(char **matrix, int row, int col);

    int x, y;   //x - row, y - col
};

#endif // PATHFINDER_H
