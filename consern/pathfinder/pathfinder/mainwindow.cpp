#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "labyrinth.h"
#include "pathfinder.h"

#include <QApplication>
#include <QPushButton>
#include <QTableWidget>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

/*Вывод лабиринта в графическом окне
 * */
void MainWindow::show_lab_win(char **lab, int row, int col)
{
    this->row = row;
    this->col = col;

    lab_win = new char *[row];
    for (int i = 0; i < row; i++) {
        lab_win[i] = new char[col];
    }

    for (int i = 0; i < row; i++) {
        for (int j = 0; j < col; j++) {
            lab_win[i][j] = lab[i][j];
        }
    }

    ui->tableWidget->setRowCount(row);
    ui->tableWidget->setColumnCount(col);


    for (int i = 0; i < row; i++) {
        for (int j = 0; j < col; j++) {
            QTableWidgetItem *item = new QTableWidgetItem(tr("%1").arg(lab[i][j]));
            item->setTextAlignment(0x0004 | 0x0080);
            ui->tableWidget->setItem(i, j, item);
        }
    }
}

/*Получение значений массива с найденным путем в классе mainwindow
 * */
void MainWindow::on_pushButton_clicked()
{
    PathFinder pathfinder;
    //Поиск кратчайшего пути или его отсутствие
    pathfinder.find_path(lab_win, row, col);

    //Вывод в таблицу и заливка ячеек с найденным путем
    QTableWidgetItem *item;

    int dx[4]={0,0,1,-1};
    int dy[4]={1,-1,0,0};
    for (int i = 0; i < row; i++) {
        for (int j = 0; j < col; j++) {
            item = new QTableWidgetItem(tr("%1").arg(lab_win[i][j]));
            if (lab_win[i][j] == '.' || lab_win[i][j] == 'S') {
                item->setTextAlignment(0x0004 | 0x0080);
                ui->tableWidget->setItem(i, j, item);
                ui->tableWidget->item(i,j)->setBackground(Qt::green);
                for (int k = 0; k < 4; k++) {
                    if (lab_win[i + dx[k]][j + dy[k]] == 'E') {
                        ui->tableWidget->setItem(i + dx[k], j + dy[k], item);
                        ui->tableWidget->item(i + dx[k],j + dy[k])->setBackground(Qt::green);
                    }
                }
            }
        }
    }
    ui->pushButton->disconnect();
}
